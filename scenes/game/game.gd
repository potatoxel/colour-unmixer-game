#region preamble
#	This is a bottle game thing.	
#
#   Copyright (C) 2021 potatoxel <look for communication methods at my website potatoxel.org>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, version 3 of the
#   License only.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#endregion preamble

extends Node

onready var Bottle = preload("res://prefabs/bottle/bottle.tscn")

var _bottle_grid_size = Vector2(4, 2)
var _bottle_size = Vector2(32, 80)
var _spacing = Vector2(30, 50)
var _bottles = []
var _current_bottle = null
var _currently_moving = false
var _completion_amount = 0
var _checkpoint

const COLORS = [
	Color.red,
	Color.blue,
	Color.violet,
	Color.pink,
	Color.green,
	Color.yellow,
	Color.magenta
]

func _ready():
	randomize()
	start_game()

func bottles_to_liquids(bottles):
	var res = []
	for bottle in bottles:
		res.append(bottle._liquids.duplicate(true))
	return res

func place_bottles_from_liquid_array(liquids_array):
	var res = []
	var liquid_id = 0
	for x in _bottle_grid_size.x:
		for y in _bottle_grid_size.y:
			var bottle = Bottle.instance()
			var liquid = liquids_array[liquid_id]
			bottle.setup(_bottle_size, liquid)
			bottle.position = Vector2(
				x * (_bottle_size.x + _spacing.x),
				y * (_bottle_size.y + _spacing.y)
			)
			bottle.connect("input_event", self, "_on_bottle_input_event", [bottle])
			_bottles.append(bottle)
			$bottles.add_child(bottle)
			liquid_id += 1
			

func start_game():
	_bottles = []
	_current_bottle = null
	_currently_moving = false
	for child in $bottles.get_children():
		child.queue_free()
	var colors = COLORS.duplicate()
	colors.shuffle()
	
	var index = 0
	for x in _bottle_grid_size.x:
		for y in _bottle_grid_size.y:
			var bottle = Bottle.instance()
			bottle.position = Vector2(
				x * (_bottle_size.x + _spacing.x),
				y * (_bottle_size.y + _spacing.y)
			)
			if not (x == 0 and y == 0):
				bottle.setup(_bottle_size, [{
					color= colors[index],
					height= 20
				}])
				index+=1
			else:
				bottle.setup(_bottle_size, [])
			bottle.connect("input_event", self, "_on_bottle_input_event", [bottle])
			_bottles.append(bottle)
			$bottles.add_child(bottle)
	
	for i in 150:
		var bottle1 = _bottles[randi() % 8]
		var bottle2 = _bottles[randi() % 8]
		
		bottle1.move_liquid_same_color_keep_some(bottle2, 5 + 2*randi()%4, 5)
	
	_checkpoint = bottles_to_liquids(_bottles)

func _on_bottle_input_event(viewport, event, shape_idx, bottle: Node2D):
	if event is InputEventMouseButton:
		if event.pressed:
			if not _currently_moving:
				if not _current_bottle:
					_current_bottle = bottle
				elif not _current_bottle == bottle and (bottle.is_empty() or _current_bottle.top_most_liquid().color == bottle.top_most_liquid().color):
					_currently_moving = true
					
					var delta = bottle.position + Vector2(-30, -50) - _current_bottle.position
					var start_point = _current_bottle.position
					
					for i in 50:
						if not _currently_moving: break
						_current_bottle.position = start_point + delta * (i/50.0)
						yield(get_tree().create_timer(0.0025),"timeout")
						if not is_instance_valid(_current_bottle): return
					for i in 30:
						if not _currently_moving: break
						_current_bottle.rotation_degrees = i
						yield(get_tree().create_timer(0.002),"timeout")
						if not is_instance_valid(_current_bottle): return
					
					if not _currently_moving: 
						_current_bottle.move_liquid_instantly_to(bottle, 1000, true)
					else:
						yield(_current_bottle.move_liquid_slowly_to(bottle, 1000, 1, true), "completed")
						
					for i in 30:
						if not _currently_moving: break
						_current_bottle.rotation_degrees = 30-i
						yield(get_tree().create_timer(0.002),"timeout")
						if not is_instance_valid(_current_bottle): return
					
					for i in 50:
						if not _currently_moving: break
						_current_bottle.position = start_point + delta * (1-i/50.0)
						yield(get_tree().create_timer(0.0025),"timeout")
						if not is_instance_valid(_current_bottle): return
						
#					var liq1 = _current_bottle.take_liquid(1000, true)
#					var liq2 = bottle.put_liquid(liq1)
#					_current_bottle.put_liquid(liq2)
					
					
					_current_bottle.position = start_point
					_current_bottle.rotation_degrees = 0
					_currently_moving = false
					_current_bottle = null
					
					
					if is_winning():
						_completion_amount += 1
						start_game()
				else:
					_current_bottle = null
			else:
				_currently_moving = false

func is_winning():
	var win = true
	for bottle in _bottles:
		if not (bottle.is_full() and bottle.is_single_color() or bottle.is_empty()):
			win = false
			break
	return win

func _on_Button_pressed():
	start_game()

func _process(delta):
	$Label.text = "Completed: " + str(_completion_amount)
	


func _on_Reset_pressed():
	for bottle in $bottles.get_children():
		bottle.queue_free()
	_bottles = []
	_current_bottle = null
	_currently_moving = false
	place_bottles_from_liquid_array(_checkpoint)
