#region preamble
#	This is a bottle game thing.	
#
#   Copyright (C) 2021 potatoxel <look for communication methods at my website potatoxel.org>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, version 3 of the
#   License only.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#endregion preamble

extends Node2D

var _size = Vector2(32, 80)
var _liquids = []
var _current_amount = 0
var _capacity = 20

func setup(size, liquids): 
	var liquids_dup = liquids.duplicate()
	liquids_dup.invert()
	put_liquid(liquids_dup)
	_size = size
	
	$CollisionShape2D.shape.extents = _size/2

func _draw():
	var current_level = 0
	
	for liquid in _liquids:
		current_level += liquid.height*(_size.y/_capacity)
		draw_rect(
			Rect2(-_size.x/2, _size.y/2-current_level, _size.x, liquid.height*(_size.y/_capacity)), 
			liquid.color
		)
	
	draw_rect(Rect2(-_size.x/2-2, -_size.y/2, 4, 80), Color.black)
	draw_rect(Rect2(_size.x/2-2, -_size.y/2, 4, 80), Color.black)
	draw_rect(Rect2(-_size.x/2-2, _size.y/2, _size.x+4, 4), Color.black)
	
	#draw_string($Label.get_font("font"), Vector2(0, _size.y/2 + 16), str(_current_amount))
	
func take_liquid(amount, same_color=false):
	if amount <= 0: return []
	var amount_left = amount
	var res = []
	var color
	for i in range(_liquids.size()-1, -1, -1):
		var liquid = _liquids[i]
		if color == null:
			color = liquid.color
		else:
			if same_color and color != liquid.color:
				break
		if (liquid.height > amount_left): 
			res.append({
				color= liquid.color,
				height= amount_left
			})
			liquid.height -= amount_left
			_current_amount -= amount_left
			amount_left = 0
		elif (liquid.height == amount_left):
			res.append({
				color= liquid.color,
				height= amount_left
			})
			_current_amount -= amount_left
			amount_left = 0
			_liquids.remove(i)
		elif (liquid.height < amount_left):
			res.append({
				color= liquid.color,
				height= liquid.height
			})
			_current_amount -= liquid.height
			amount_left -= liquid.height
			_liquids.remove(i)
		if(amount_left == 0):
			break
	update()
	return res

func put_liquid(liquids: Array):
	liquids = liquids.duplicate()
	var empty_space = _capacity - _current_amount
	if empty_space == 0:
		return liquids
	for i in range(liquids.size()-1, -1, -1):
		var liquid = liquids[i]
		var top_liquid = _liquids[_liquids.size()-1] if _liquids.size() > 0 else null
		
		if liquid.height > empty_space:
			liquid.height -= empty_space
			if top_liquid and top_liquid.color == liquid.color:
				top_liquid.height += empty_space
			else:
				_liquids.append({
					color= liquid.color,
					height= empty_space
				})
			_current_amount += empty_space
			empty_space = 0
		elif liquid.height == empty_space:
			liquids.remove(i)
			if top_liquid and top_liquid.color == liquid.color:
				top_liquid.height += empty_space
			else:
				_liquids.append({
					color= liquid.color,
					height= empty_space
				})
			_current_amount += empty_space
			empty_space = 0
		elif liquid.height < empty_space:
			liquids.remove(i)
			if top_liquid and top_liquid.color == liquid.color:
				top_liquid.height += liquid.height
			else:
				_liquids.append({
					color= liquid.color,
					height= liquid.height
				})
			_current_amount += liquid.height
			empty_space -= liquid.height
		if empty_space == 0:
			break
	update()
	return liquids

func top_most_liquid():
	return _liquids[_liquids.size()-1]

func is_full():
	return _current_amount >= _capacity

func is_empty():
	return _current_amount == 0

func is_single_color():
	return _liquids.size() == 1

func move_liquid_instantly_to(bottle, amount, same_color = false):
	var liq1 = take_liquid(amount, same_color)
	var liq2 = bottle.put_liquid(liq1)
	put_liquid(liq2)

func move_liquid_same_color_keep_some(bottle, amount, minimum):
	if is_empty(): return
	move_liquid_instantly_to(bottle, min(amount, top_most_liquid().height-minimum), true)

func move_liquid_slowly_to(bottle, amount, speed, same_color = false):
	var color
	if not is_empty():
		color = top_most_liquid().color
	else: 
		return
	for i in amount/speed:
		if is_empty() or not top_most_liquid().color == color:
			return
		var res = move_liquid_instantly_to(bottle, speed, same_color)
		yield(get_tree().create_timer(0.01),"timeout")
		if bottle.is_full() or is_empty():
			return

